#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2013 Asumi Kamikaze Inc.
# Copyright (c) 2013 The Octopus Apps Inc.
# Licensed under the Apache License, Version 2.0 (the "License")
# Author: Alejandro M. Bernardis
# Email: alejandro.bernardis at gmail.com
# Created: 21/06/2013 09:01

import os
 
 
# root
ROOT_PATH = os.path.abspath(os.path.dirname(__file__))
 
 
# port
DEBUG = True
PORT = 8000
SSL = False
CDN = False
CDN_PREFIX = None
PREFORK_PROCESS = 0
XSRF_COOKIE = True
COOKIE_SECRET = os.urandom(36)
COOKIE_USER_SESSION = u'user_session'
LOGIN_URL = u'/auth/login'
 
 
# handlers
HANDLERS_PKG = u'module.handlers'
HANDLERS_LIST = ('trending',)
 
 
# session
SESSION = dict(
    engine=u'memcached',
    servers=(u'127.0.0.1:11211',),
    serializer=u'marshal',
    username=u'memsupport',
    password=u'',
)
 
 
# database (default)
DATABASE_NAME = u'pypi_rank'
DATABASE_HOST = u'localhost'
DATABASE_PORT = 5432
DATABASE_CONN = 100
DATABASE_USER = u'postgres'
DATABASE_PASS = u'postgres'
 
 
# graph (social)
GRAPH_SOCIAL_NAME = u'social'
GRAPH_SOCIAL_HOST = u'127.0.0.1'
GRAPH_SOCIAL_PORT = 8529
GRAPH_SOCIAL_USER = u'graphsupport'
GRAPH_SOCIAL_PASS = u''
 
 
# queues
RABBITMQ_TRACK_NAME = u'tracking'
RABBITMQ_TRACK_BROK = u'amqp://guest:guest@127.0.0.1//'
 
 
# email
EMAIL_ACCOUNT = u''
EMAIL_USER = u''
EMAIL_PASS = u''
EMAIL_HOST = u'smtp.google.com'
EMAIL_PORT = 587
EMAIL_TLS = True
 
 
# site
SITE_TITLE = u'PyPi Ranking'
SITE_DESCRIPTION = u''
SITE_DOMAIN = u'127.0.0.1'
SITE_ROOT = u'/pypi_rank'
SITE_STATIC = SITE_ROOT + u'static'
 
 
# paths
CA_PATH = os.path.join(ROOT_PATH, u'CA')
STATIC_PATH = os.path.join(ROOT_PATH, u'static')
TEMPLATES_PATH = os.path.join(ROOT_PATH, u'templates')
TEMP_PATH = u'/tmp'
DATA_PATH = os.path.join(ROOT_PATH, u'data')
 
 
# autoreload
AUTORELOAD_ENABLED = True
AUTORELOAD_FILES = ()
 
 
# static files
STATIC_FILES = ()


#urls
LIST_PACKAGE_URL="https://pypi.python.org/pypi?%3Aaction=index"
PYPI_INDEX="https://pypi.python.org"

#delta
DELTA=1

#log
from tornado.log import app_log
APP_LOG=app_log

#page count
PAGE_COUNT=25
