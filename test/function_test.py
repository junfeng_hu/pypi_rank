import sys
import json
import unittest

sys.path.append('..')

import utils

class ConfigDbTest(unittest.TestCase):
    def setUp(self):
        self.conn=utils.get_db_conn()
        self.packages=utils.get_package_urls()[0:10]
    def tearDown(self):
        self.conn.close()
    def test_fetch_and_insert(self):
        results=utils.work_do_get_data(self.packages)
        for r in results:
            json.loads(r[2])
        cursor=self.conn.cursor()
        insert_sql="INSERT INTO package(name,url,info) VALUES (%s,%s,%s)"
        rl=cursor.executemany(insert_sql,results)
        #self.assertEqual(rl,len(results))
        cursor.close()


if __name__ == '__main__':
    unittest.main()




