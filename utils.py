#!---coding:utf-8---
import time
import logging
import os.path
import urllib2
from multiprocessing.dummy import Pool
import json

import lxml
import lxml.html

import psycopg2
import psycopg2.extras

import settings



time_format=u'%Y/%m/%d %H:%M:%S'
def get_current_time_str():
    return time.strftime(time_format, time.localtime())


def get_package_urls(r):
    html=lxml.html.fromstring(r.body)

    list_table=html.find_class("list")
    if not list_table:
        settings.APP_LOG.error("ERROR.No talbe with class list.")
        raise "No table with class list."
    table=list_table[0]
    table_iter=table.getiterator()

    package_urls=[]
    for t in table_iter:
        if t.tag=='a':
            url=t.get('href')
            package_urls.append(settings.PYPI_INDEX+url+"/json")
    return package_urls


data_json_dir = os.path.join(settings.DATA_PATH,u'json')
def get_package_info(r):
    info=json.loads(r.body)["info"]
    package_url=info["package_url"]
    description=info["description"]
    downloads=info["downloads"]
    last_month_downloads=downloads["last_month"]
    last_week_downloads=downloads["last_week"]
    last_day_downloads=downloads["last_day"]
    name=info["name"]
    home_page=info["home_page"]
    data=dict(
            package_url=package_url,
            description=description,
            last_month_downloads=last_month_downloads,
            last_week_downloads=last_week_downloads,
            last_day_downloads=last_day_downloads,
            name=name,
            home_page=home_page
            )
    return data

'''def work_do_get_data(package_urls):
    pool=Pool(8)
    results=pool.map(get_package_info,package_urls)
    pool.close()
    pool.join()
    return results
'''


def get_db_conn(cursor_dict=False):
    conn=psycopg2.connect(
            database=settings.DATABASE_NAME,
            user=settings.DATABASE_USER,
            password=settings.DATABASE_PASS,
            host=settings.DATABASE_HOST,
            port=settings.DATABASE_PORT,
            cursor_factory=psycopg2.extras.DictCursor if cursor_dict else None
            )
    conn.autocommit=True
    #conn.set_client_encoding('gbk')
    return conn


def insert_package_list(package_urls):
    conn = get_db_conn()    
    cursor=conn.cursor()
    name_and_package_urls=set([(pu.rsplit('/',3)[1],pu.rsplit('/',2)[0]) for pu in package_urls])
    find_sql="SELECT name,package_url FROM package;"
    cursor.execute(find_sql)
    ep_set=set(cursor.fetchall())
    join_set=name_and_package_urls.intersection(ep_set)
    delete_sql="DELETE FROM package WHERE name=%s AND package_url=%s;"
    delete_set=ep_set-join_set
    cursor.executemany(delete_sql,delete_set)
    insert_set=name_and_package_urls-join_set
    insert_sql="INSERT INTO package(name,package_url) VALUES (%s,%s);"
    cursor.executemany(insert_sql,insert_set)
    conn.commit()
    cursor.close()
    conn.close()



