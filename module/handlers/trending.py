#!---coding:utf-8---

import json
import psycopg2
from tornado.web import RequestHandler,HTTPError,asynchronous
from tornado.gen import coroutine



class TrendingHandler(RequestHandler):
    @asynchronous
    @coroutine
    def get(self):
        since=self.get_argument('since',default='last_day')
        page=int(self.get_argument('page',default='1'))
        if since not in ('last_day','last_month','last_week') or page <= 0:
            raise HTTPError(400)
        trending_sql="SELECT * FROM package ORDER BY %s DESC LIMIT %s OFFSET %s;" %(
                since,self.settings.get('page_count'),(page-1)*self.settings.get('page_count')
                )
        conn=self.settings.get('conn')
        cursor=conn.cursor()
        cursor.execute(trending_sql)
        packages=cursor.fetchall()
        cursor.close()
        '''if page >= 2:
            for p in packages:
                p['update_time']=str(p.get('update_time',''))
                self.write(dict(p))
            self.flush()
            self.finish()
        else:
            self.render('trending.html', packages=packages)
        '''
        self.render('trending.html', packages=packages, page=page, since=since)


class About(RequestHandler):
    def get(self):
        self.render('about.html')


handlers_list=[
        (r'/trending',TrendingHandler),
        (r'/about', About),
        ]

