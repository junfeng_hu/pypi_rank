#!---coding:utf-8---
import sys
import traceback
import logging
import datetime
import time
sys.path.append("..")
import utils
import settings

from tornado.ioloop import IOLoop
import tornado.curl_httpclient
from tornado.httpclient import AsyncHTTPClient,HTTPRequest,HTTPError
from tornado.gen import coroutine

@coroutine
def update_package_list():
    settings.APP_LOG.setLevel(logging.INFO)

    client = AsyncHTTPClient()
    request=HTTPRequest(url=settings.LIST_PACKAGE_URL,
            connect_timeout=200.0, request_timeout=200.0,
            #proxy_host='127.0.0.1',proxy_port=8087
            )
    try:
        r= yield client.fetch(request)
        package_urls=utils.get_package_urls(r)
        utils.insert_package_list(package_urls)
    except Exception as e:
        trace=traceback.format_exc()
        settings.APP_LOG.error("%s\n%s,%s" % (trace,settings.LIST_PACKAGE_URL,str(e)))

    settings.APP_LOG.info("update_package_list call successfully")
    IOLoop.instance().add_callback(update_package_info)

    




    
@coroutine
def update_package_info():
    settings.APP_LOG.setLevel(logging.INFO)

    update_sql="UPDATE package SET description=%s,last_month=%s,last_week=%s,last_day=%s,home_page=%s,update_time=%s WHERE id=%s;"    
    conn=utils.get_db_conn()
    cursor=conn.cursor()
    cursor.execute("SELECT id,package_url,update_time FROM package;")
  
    packages=cursor.fetchall()
    client = AsyncHTTPClient()
    start=time.time()
    for i,package in enumerate(packages):
        if datetime.datetime.utcnow()-package[2]<datetime.timedelta(days=1):
            continue
        request=HTTPRequest(url=package[1]+'/json',
            #connect_timeout=1.0, request_timeout=1.0,
            #proxy_host='127.0.0.1',proxy_port=8087
            )
        try:
            r= yield client.fetch(request)
            if r.code==200:
                pd=utils.get_package_info(r)
                #settings.APP_LOG.setLevel(logging.INFO)
                #settings.APP_LOG.info("update %d : %s" % (i,package[1]))
                pd['description']=pd['description'].encode("utf-8") if pd['description'] else ''
                
                cursor.execute(
                    update_sql,
                    (
                        pd['description'],
                        pd['last_month_downloads'],
                        pd['last_week_downloads'],
                        pd['last_day_downloads'],
                        pd['home_page'],
                        datetime.datetime.utcnow(),
                        package[0]
                        )
                    )
        except Exception as e:
            trace=traceback.format_exc()
            settings.APP_LOG.error("%s\n%s,%s" % (trace,package[1],str(e)))
    settings.APP_LOG.info("update all packages info using time:%f" %(time.time()-start))
    cursor.close()
    conn.commit()
    conn.close()





