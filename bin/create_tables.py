import sys
sys.path.append('..')

import utils


conn = utils.get_db_conn()

def create_tables():
    create_sql='''
    CREATE TABLE package (
    id SERIAL PRIMARY KEY,
    name VARCHAR(1000) NOT NULL UNIQUE,
    package_url VARCHAR(1000) NOT NULL,
    description TEXT DEFAULT '',
    last_month INT DEFAULT 0,
    last_week INT DEFAULT 0,
    last_day INT DEFAULT 0,
    home_page VARCHAR(1000),
    update_time TIMESTAMP NOT NULL DEFAULT NOW()
    );
    '''
    cursor=conn.cursor()
    cursor.execute("DROP TABLE IF EXISTS package;")
    cursor.execute(create_sql)
    conn.commit()
    cursor.close()



if __name__=="__main__":
    create_tables()
    conn.close()

