import sys
import json
import glob
import os.path
from tornado.httpclient import HTTPClient

sys.path.append('..')
import settings
import utils


def update_all_package(conn,flist):
    cursor=conn.cursor()
    update_sql="UPDATE package SET description=%s,last_month=%s,last_week=%s,last_day=%s,home_page=%s WHERE name=%s;"
    for f in flist:
        f=open(f,"rb")
        pd=json.load(f)
        f.close()
        print ("update package %s" % pd['name'])
        cursor.execute(
                update_sql,
                (
                    pd['description'],
                    pd['last_month_downloads'],
                    pd['last_week_downloads'],
                    pd['last_day_downloads'],
                    pd['home_page'],
                    pd['name']
                    )
                )

    conn.commit()
    cursor.close()
        
    


if __name__=="__main__":
    client=HTTPClient()
    r=client.fetch(settings.LIST_PACKAGE_URL)
    package_urls=utils.get_package_urls(r)
    utils.insert_package_list(package_urls)
    conn.autocommit=True
    flist=glob.glob(os.path.join(utils.data_json_dir,"*.json"))
    update_all_package(conn,flist)
    conn.close()

