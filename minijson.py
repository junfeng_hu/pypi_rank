#!/usr/bin/env python2
#!---coding:utf-8---
import os.path
import json
import glob
from pypi_rank import utils

SAVE_DIR=os.path.abspath("minijson")
INPUT_DIR=os.path.abspath(".")

conn=utils.get_db_conn()

SAVE_FLAG=True

def insert_packages_info(flist):
    for f in flist:
        fpath=os.path.join(INPUT_DIR,f)
        if os.path.isfile(fpath):
            print ("processing %s" % f)
            fin=open(fpath,"rb")
            package=json.loads(fin.read())
            fin.close()
            info=package["info"]
            package_url=info["package_url"]
            description=info["description"]
            downloads=info["downloads"]
            last_month_downloads=downloads["last_month"]
            last_week_downloads=downloads["last_week"]
            last_day_downloads=downloads["last_day"]
            name=info["name"]
            home_page=info["home_page"]
            data=dict(
                    package_url=package_url,
                    description=description,
                    last_month_downloads=last_month_downloads,
                    last_week_downloads=last_week_downloads,
                    last_day_downloads=last_day_downloads,
                    name=name,
                    home_page=home_page
                    )
            if SAVE_FLAG==True:
                fout=open(os.path.join(SAVE_DIR,name+".json"),"wb")
                fout.write(json.dumps(data))
                fout.close()


if __name__=="__main__":
    flist=glob.glob("json/*.json")
    insert_packages_info(flist)

