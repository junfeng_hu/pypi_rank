import os.path
import time
import sqlite3

import settings
import utils

DBFILE=os.path.join(settings.DATA_PATH,'pypi_rank.db')
def create_tables():
    create_sql='''
    CREATE TABLE IF NOT EXISTS packages (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL UNIQUE,
    package_url TEXT NOT NULL,
    description TEXT DEFAULT '',
    last_month INTEGER DEFAULT 0,
    last_week INTEGER DEFAULT 0,
    last_day INTEGER DEFAULT 0,
    home_page TEXT DEFAULT '',
    update_time TIMESTAMP DEFAULT ''
    );
    '''
    conn=sqlite3.connect(DBFILE,detect_types=sqlite3.PARSE_DECLTYPES,
            isolation_level=None)
    cursor=conn.cursor()
    cursor.execute(create_sql)
    conn.commit()
    conn.close()


def convert():
    pgconn=utils.get_db_conn(cursor_dict=True)
    sqconn=sqlite3.connect(DBFILE,detect_types=sqlite3.PARSE_DECLTYPES,
            isolation_level=None)
    sqconn.text_factory = str
    pgcursor=pgconn.cursor()
    sqcursor=sqconn.cursor()
    pgcursor.execute("SELECT * FROM package;")
    while True:
        p=pgcursor.fetchone()
        if not p:
            break
        insert_sql="INSERT INTO packages VALUES(?,?,?,?,?,?,?,?,?)"
        print ("insert %s" % p['name'])
        sqcursor.execute(
                insert_sql,
                (
                    None,
                    p['name'],
                    p['package_url'],
                    p['description'],
                    p['last_month'],
                    p['last_week'],
                    p['last_day'],
                    p['home_page'],
                    p['update_time']
                )
            )
    sqconn.commit()
    sqcursor.close()
    sqconn.close()
    pgcursor.close()
    pgconn.close()

if __name__=="__main__":
    create_tables()
    convert()

    


