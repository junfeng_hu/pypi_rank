#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2013 Asumi Kamikaze Inc.
# Copyright (c) 2013 The Octopus Apps Inc.
# Licensed under the Apache License, Version 2.0 (the "License")
# Author: Alejandro M. Bernardis
# Email: alejandro.bernardis at gmail.com
# Created: 21/06/2013 09:01
from __future__ import print_function 
import os
import sys
import logging
import functools
import settings
import utils
from module.task_timer import update_package_list
 
 
# bootstrap
 
_parent_path = os.path.split(settings.ROOT_PATH)[0]
 
for item in ['bin', 'lib']:
    sys.path.insert(0, os.path.join(_parent_path, item))
 
 
# version
 
__project_name__ = u'pypi_rank'
__project_full_name__ = u'pypi ranking'
__project_owner__ = u'junfeng'
__project_author__ = u'junfeng'
__project_version__ = (0, 0, 1, 'alpha', 0)
__project_created__ = u'2014/01/30 23:17:56'


 
# imports
 

from tornado.autoreload import watch as autoreload_watch, \
    start as autoreload_start
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop,PeriodicCallback
from tornado.web import Application, StaticFileHandler
from tornado.options import define, options, parse_command_line
 
 
# server
 
define('debug', default=settings.DEBUG)
define('port', default=settings.PORT)
define('ssl', default=settings.SSL)
define('cdn', default=settings.CDN)
define('cdn_prefix', default=settings.CDN_PREFIX)
define('prefork_process', default=settings.PREFORK_PROCESS)
define('xsrf_cookie', default=settings.XSRF_COOKIE)
define('cookie_secret', default=settings.COOKIE_SECRET)
define('cookie_user_session', default=settings.COOKIE_USER_SESSION)
define('autoreload', default=settings.AUTORELOAD_ENABLED)
 
 
# database
 
define('database_name', default=settings.DATABASE_NAME)
define('database_host', default=settings.DATABASE_HOST)
define('database_port', default=settings.DATABASE_PORT)
define('database_conn', default=settings.DATABASE_CONN)
define('database_user', default=settings.DATABASE_USER)
define('database_pass', default=settings.DATABASE_PASS)
 
 
# graph
 
define('graph_name', default=settings.GRAPH_SOCIAL_NAME)
define('graph_host', default=settings.GRAPH_SOCIAL_HOST)
define('graph_port', default=settings.GRAPH_SOCIAL_PORT)
define('graph_user', default=settings.GRAPH_SOCIAL_USER)
define('graph_pass', default=settings.GRAPH_SOCIAL_PASS)
 
 
# async database
 
define('database_pool', default='%s_pool' % settings.DATABASE_NAME)
 
 
# application
 
class MainApplication(Application):
    def __init__(self):
        _handlers = []
 
        for item in settings.STATIC_FILES:
            _handlers.append((item, StaticFileHandler,
                              dict(path=settings.STATIC_PATH)))
 
        for item in settings.HANDLERS_LIST:
            _name = '%s.%s' % (settings.HANDLERS_PKG, item)
            _module = __import__(_name, globals(), locals(), [item], 0)
            _handlers.extend(_module.handlers_list)
 
        for item in settings.AUTORELOAD_FILES:
            autoreload_watch(os.path.join(settings.ROOT_PATH, item))
 
        _database = dict(
            database=options.database_name,
            host=options.database_host,
            port=options.database_port,
            max_pool_size=options.database_conn,
            username=options.database_user,
            password=options.database_pass,
        )
 
        _graph = dict(
            database=options.graph_name,
            host=options.graph_host,
            port=options.graph_port,
            username=options.graph_user,
            password=options.graph_pass,
        )
 
        _settings = dict(
            debug=options.debug,
            ssl=options.ssl,
            cdn=options.cdn,
            cdn_prefix=options.cdn_prefix,
            xsrf_cookies=options.xsrf_cookie,
            cookie_secret=options.cookie_secret,
            cookie_user_session=options.cookie_user_session,
            login_url=settings.LOGIN_URL,
            session=settings.SESSION,
            path=settings.ROOT_PATH,
            static_path=settings.STATIC_PATH,
            template_path=settings.TEMPLATES_PATH,
            temp_path=settings.TEMP_PATH,
            ca_path=settings.CA_PATH,
            site_title=settings.SITE_TITLE,
            site_description=settings.SITE_DESCRIPTION,
            site_domain=settings.SITE_DOMAIN,
            site_root=settings.SITE_ROOT,
            database=_database,
            graph=_graph,
            conn=utils.get_db_conn(cursor_dict=True),
            page_count=settings.PAGE_COUNT
            )
 
        super(MainApplication, self).__init__(_handlers, **_settings)
 
    @property
    def ssl_support(self):
        return self.settings.get('ssl', False)
 
    def ssl_config(self):
        import ssl
        ca_path = self.settings.get('ca_path', settings.CA_PATH)
        if self.ssl_support and os.path.isdir(ca_path):
            return dict(
                cert_reqs=ssl.CERT_REQUIRED,
                certfile=os.path.join(ca_path, 'server.crt'),
                keyfile=os.path.join(ca_path, 'server.key'),
                ca_certs=os.path.join(ca_path, 'cacert.crt')
            )
        return None
 
    @property
    def conn(self):
        return self.settings['conn']

        
 
# Main
 
if __name__ == '__main__':
    try:
        
        parse_command_line()
        app = MainApplication()
        config = dict(xheaders=True, ssl_options=app.ssl_config())
        http_server = HTTPServer(app, **config)
        if options.prefork_process > 1:
            http_server.bind(options.port)
            http_server.start(options.prefork_process)
        else:
            http_server.listen(options.port)

        io_loop = IOLoop.instance()
        timer1=PeriodicCallback(update_package_list,24*60*60*1000,io_loop)
        if app.settings.get('debug')!=True:
            io_loop.run_sync(update_package_list)
        timer1.start()
        if options.autoreload:
            autoreload_start(io_loop)
        io_loop.start()
    except KeyboardInterrupt:
        print ("Exiting tornado server...")
        timer1.stop()
        io_loop.close()
        sys.exit(0)
